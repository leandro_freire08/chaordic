<?php

use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Di\FactoryDefault;

$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

$di->set('mongo', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $mongo = new MongoClient($dbConfig['host']);
    return $mongo->selectDB($dbConfig['dbname']);
}, true);

$di->set('collectionManager', function () {
    return new Phalcon\Mvc\Collection\Manager();
}, true);

$di->set('short', function () {
    return new Chaordic\Services\ShortUrlService();
}, true);
