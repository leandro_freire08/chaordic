<?php

defined('APP_PATH') || define('APP_PATH', realpath('../app'));

return new \Phalcon\Config(array(

    'database' => array(
        'host'       => 'mongodb://localhost:27017',
        'username'   => '',
        'password'   => '',
        'dbname'     => 'chaordic'
    ),

    'application' => array(
        'modelsDir'      => APP_PATH . '/models/',
        'controllersDir' => APP_PATH . '/controllers/',
        'servicesDir'    => APP_PATH . '/services/',
        'validatorsDir'  => APP_PATH . '/validators/',
        'baseUri'        => '/api/',
    )
));
