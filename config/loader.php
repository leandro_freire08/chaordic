<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    array(
        "Chaordic"    => "../app/",
        "Chaordic\\Controller" => "../app/controllers/",
        "Chaordic\\Models" => "../app/models/",
        "Chaordic\\Services" => "../app/services/",
        "Chaordic\\Validators" => "../app/validators/"
    )
);

$loader->registerDirs(
    array(
        $config->application->modelsDir,
        $config->application->controllersDir,
        $config->application->servicesDir,
        $config->application->validatorsDir
    )
)->register();
