#!/bin/bash

# Again set the right permissions (needed when mounting from a volume)
chown -R www-data:www-data /chaordic
chmod 755 /chaordic -R

/usr/bin/supervisord -n -c /etc/supervisord.conf