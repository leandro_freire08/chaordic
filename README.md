# URL shorter API

## You can run the application on Docker :)

### Docker

Build docker container
```sh
$ docker build -t chaordic-api .
```

Running container and expose Nginx port
```sh
$ docker run --name api -p 8080:80 -p 27018:27017 -d chaordic-api
```

You can now point the browser to http://localhost:8080 :)
The mongo connect will be available through mongodb://localhost:27018

Running tests under docker container
```sh
$ docker exec container_id php /chaordic/vendor/bin/phpunit -c /chaordic/tests/phpunit.xml --bootstrap /chaordic/tests/TestHelper.php
```

Running coverage report
```sh
docker exec container_id php /chaordic/vendor/bin/phpunit -c /chaordic/tests/phpunit.xml --bootstrap /chaordic/tests/TestHelper.php --coverage-html /chaordic/public/coverage /chaordic/tests
```

The report will be available at http://localhost:8080/coverage/index.html

## Or if you preffer, you can install the dependencies and run locally: 

### Dependencies
- [Nginx](http://www.nginx.com)
- [PHP >= 5.6](http://php.net)
- [Phalcon = 2.0.10](http://phalconphp.com)
- [Composer = latest](https://getcomposer.org)
- [PHPUnit >= 5.2](https://phpunit.de/)
- [MongoDB >= 3.2.x](<https://www.mongodb.org)

### PHP Version
I used php version 5.6.19 under development. To install php-5.6 use the following steps:

```sh
$ sudo add-apt-repository ppa:ondrej/php5-5.6
$ sudo apt-get update
$ sudo apt-get install python-software-properties
$ sudo apt-get install php5
```

### PHP libraries
The application was tested under Ubuntu 14.04, and the following libraries are required:

- php5
- php5-apcu
- php5-cgi
- php5-cli
- php5-common
- php5-curl
- php5-dev
- php5-fpm
- php5-json
- php5-mongo
- php5-xdebug

### Installation
```sh
$ git clone git@bitbucket.org:leandro_freire08/chaordic.git
$ cd chaordic
$ composer install
```

### Configuring
Copy nginx virtual host file (chaordic) under deploy folder to sites-enabled folder of nginx server. i.e: /etc/nginx/sites-enabled
Add host to /etc/hosts, i.e: 127.0.0.1	charodic.dev

### Testing
```sh
$ cd chaordic/tests
$ phpunit -c phpunit.xml --bootstrap TestHelper.php
```

### Coverage
The application have 100% unit test coverage, to see report:

```sh
$ cd chaordic/tests
$ phpunit --coverage-html <output_folder> .
```
Open index.html under <output_folder>

### PSR-2
The application follow all PSR-2 standards
```sh
$ cd chaordic/app
$ phpcs --standard=PSR2 --colors .
```