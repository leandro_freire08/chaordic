<?php

namespace Test\Controllers;

use Chaordic\Controllers\StatisticController;
use Chaordic\Models\Url;
use Phalcon\Http\Response;
use Test\Fixtures\UrlFixture;

class StatisticControllerTest extends \UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->di->set('response', function () {
            return new Response();
        });
    }

    public function testIndexAction()
    {
        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $controller = new StatisticController();
        $response = $controller->indexAction();
        $this->assertEquals('200 OK', $response->getStatusCode());

        $responseData = json_decode($response->getContent());
        $this->assertEquals(1, $responseData->data[0]->urlCount);
        $this->assertEquals(0, $responseData->data[0]->hits);
        $this->assertObjectHasAttribute('topUrls', $responseData->data[0]);
    }

    public function testShowAction()
    {
        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $controller = new StatisticController();
        $response = $controller->showAction($url->getId());
        $this->assertEquals('200 OK', $response->getStatusCode());

        $responseData = json_decode($response->getContent());
        $this->assertObjectHasAttribute('id', $responseData->data);
    }

    public function testShowActionInvalidId()
    {
        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $controller = new StatisticController();
        $response = $controller->showAction($url->getId() . 'x');
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }
}
