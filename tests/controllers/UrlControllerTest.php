<?php

namespace Test\Controllers;

use Chaordic\Controllers\UrlController;
use Chaordic\Models\Url;
use Phalcon\Http\Response;
use Test\Fixtures\UrlFixture;

class UrlControllerTest extends \UnitTestCase
{
    protected $id;

    public function setUp()
    {
        parent::setUp();

        $this->di->set('response', function () {
            return new Response();
        });

        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $this->id = $url->getId();
    }

    public function testRedirectAction()
    {
        $controller = new UrlController();
        $response = $controller->redirectAction($this->id);
        $this->assertEquals('301 Moved Permanently', $response->getStatusCode());
        $url = new Url();
        $urlResult = $url->findById(new \MongoId($this->id));
        $this->assertEquals(1, $urlResult->hits);
    }

    public function testRedirectActionUrlInvalidId()
    {
        $controller = new UrlController();
        $response = $controller->redirectAction($this->id . "x");
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }

    public function testRedirectActionUrlNotFound()
    {
        $controller = new UrlController();
        $response = $controller->redirectAction("56e9a3be18c7dc897d8b4569");
        $this->assertEquals('404 Not Found', $response->getStatusCode());
    }

    public function testDestroyAction()
    {
        $controller = new UrlController();
        $response = $controller->destroyAction($this->id);
        $this->assertEquals('204 No Content', $response->getStatusCode());
        $url = new Url();
        $urlResult = $url->findById(new \MongoId($this->id));
        $this->assertFalse($urlResult);
    }

    public function testDestroyActionUrlInvalidId()
    {
        $controller = new UrlController();
        $response = $controller->destroyAction($this->id . "x");
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }

    public function testDestroyActionUrlNotFound()
    {
        $controller = new UrlController();
        $response = $controller->destroyAction("56e9a3be18c7dc897d8b4569");
        $this->assertEquals('404 Not Found', $response->getStatusCode());
    }
}
