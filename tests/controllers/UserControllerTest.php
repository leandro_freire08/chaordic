<?php

namespace Test\Controllers;

use Chaordic\Controllers\UserController;
use Chaordic\Models\Url;
use Chaordic\Models\User;
use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Test\Fixtures\UrlFixture;

class UserControllerTest extends \UnitTestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->di->set('response', function () {
            return new Response();
        });

        $this->di->set('request', function () {
            $requestMock = $this->getMockBuilder('Phalcon\Http\Request')->getMock();
            $requestMock->expects($this->any())->method('getJsonRawBody')->willReturn($this->user);
            return $requestMock;
        });

        $user = array("id" => "test");
        $this->user = (object) $user;
    }

    public function testCreateAction()
    {
        $controller = new UserController();
        $response = $controller->createAction();
        $this->assertEquals('201 Created', $response->getStatusCode());
        $user = new User();
        $userResult = $user->find(array(array('name' => 'test')));
        $this->assertNotNull($userResult);
        $this->assertEquals('test', $userResult[0]->name);
    }

    public function testCreateActionValidationFailure()
    {
        $this->user = (object) array("xx" => "test");
        $controller = new UserController();
        $response = $controller->createAction();
        $this->assertEquals('400 Bad Request', $response->getStatusCode());
    }

    public function testCreateActionErrorOnSave()
    {
        $this->user = (object) array("id" => -1);
        $controller = new UserController();
        $response = $controller->createAction();
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }

    public function testCreateUrlAction()
    {
        $user = new User();
        $user->populate($this->user);
        $user->save();

        $this->user = (object) array("url" => "http://test.com");
        $controller = new UserController();
        $response = $controller->createUrlAction($user->name);
        $this->assertEquals('201 Created', $response->getStatusCode());
    }

    public function testCreateUrlActionErrorOnSave()
    {
        $user = new User();
        $user->populate($this->user);
        $user->save();

        $this->user = (object) array("url" => -1);
        $controller = new UserController();
        $response = $controller->createUrlAction($user->name);
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }

    public function testCreateUrlActionValidationFailure()
    {
        $user = new User();
        $user->populate($this->user);
        $user->save();
        $this->user = (object) array("xx" => "test");
        $controller = new UserController();
        $response = $controller->createUrlAction($user->name);
        $this->assertEquals('400 Bad Request', $response->getStatusCode());
    }

    public function testCreateUrlActionUserNotFound()
    {
        $controller = new UserController();
        $response = $controller->createUrlAction('xxx');
        $this->assertEquals('404 Not Found', $response->getStatusCode());
    }

    public function testStatsAction()
    {
        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $controller = new UserController();
        $response = $controller->statsAction($urlFixture->userId);
        $this->assertEquals('200 OK', $response->getStatusCode());
        $responseData = json_decode($response->getContent());
        $this->assertObjectHasAttribute('url', $responseData->data[0]);
        $this->assertObjectHasAttribute('hits', $responseData->data[0]);
        $this->assertObjectHasAttribute('shortUrl', $responseData->data[0]);
        $this->assertObjectHasAttribute('id', $responseData->data[0]);
    }

    public function testStatsActionNotFound()
    {
        $controller = new UserController();
        $response = $controller->statsAction('xxx');
        $this->assertEquals('404 Not Found', $response->getStatusCode());
    }

    public function testDestroyAction()
    {
        $user = new User();
        $user->populate($this->user);
        $user->save();

        $controller = new UserController();
        $response = $controller->destroyAction($user->getId());
        $this->assertEquals('204 No Content', $response->getStatusCode());
        $user = new User();
        $userResult = $user->findById(new \MongoId($user->getId()));
        $this->assertFalse($userResult);
    }

    public function testDestroyActionUserInvalidId()
    {
        $id = new \MongoId();
        $controller = new UserController();
        $response = $controller->destroyAction($id->__toString() . "x");
        $this->assertEquals('500 Internal Server Error', $response->getStatusCode());
    }

    public function testDestroyActionUserNotFound()
    {
        $controller = new UserController();
        $response = $controller->destroyAction("56e9a3be18c7dc897d8b4569");
        $this->assertEquals('404 Not Found', $response->getStatusCode());
    }
}
