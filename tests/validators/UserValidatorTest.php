<?php

namespace Test\Validators;

use Chaordic\Validators\UserValidator;

class UserValidatorTest extends \UnitTestCase
{
    public function testInitialize()
    {
        $validator = new UserValidator();
        $userData = new \stdClass();
        $userData->id = 'test';
        $this->assertInstanceOf('Chaordic\Validators\UserValidator', $validator->initialize('user', $userData));
        $this->assertInstanceOf('\ArrayIterator', $validator->getErrors());
    }

    public function testValidate()
    {
        $validator = new UserValidator();
        $userData = new \stdClass();
        $userData->id = "test";
        $validator->initialize('user', $userData)->validate('id');
        $this->assertEmpty($validator->getErrors());
    }

    public function testValidateNullData()
    {
        $validator = new UserValidator();
        $userData = new \stdClass();
        $validator->initialize('user', $userData)->validate('id');
        $this->assertEquals("Invalid format", $validator->getErrors()->current());
    }

    public function testValidateInvalidProperty()
    {
        $validator = new UserValidator();
        $userData = new \stdClass();
        $userData->invalid = "invalid";
        $validator->initialize('user', $userData)->validate('id');
        $this->assertEquals(
            "user must contain id property",
            $validator->getErrors()->current()
        );
    }

    public function testValidateExceededStructure()
    {
        $validator = new UserValidator();
        $userData = new \stdClass();
        $userData->id = "test";
        $userData->invalid = "invalid";
        $validator->initialize('user', $userData)->validate('id');
        $this->assertEquals(
            "user must contain only id property",
            $validator->getErrors()->current()
        );
    }
}
