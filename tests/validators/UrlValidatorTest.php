<?php

namespace Test\Validators;

use Chaordic\Validators\UrlValidator;

class UrlValidatorTest extends \UnitTestCase
{
    public function testInitialize()
    {
        $validator = new UrlValidator();
        $urlData = new \stdClass();
        $urlData->url = 'url';
        $this->assertInstanceOf('Chaordic\Validators\UrlValidator', $validator->initialize('url', $urlData));
        $this->assertInstanceOf('\ArrayIterator', $validator->getErrors());
    }

    public function testValidate()
    {
        $validator = new UrlValidator();
        $urlData = new \stdClass();
        $urlData->url = 'url';
        $validator->initialize('url', $urlData)->validate('url');
        $this->assertEmpty($validator->getErrors());
    }

    public function testValidateNullData()
    {
        $validator = new UrlValidator();
        $urlData = new \stdClass();
        $validator->initialize('url', $urlData)->validate('url');
        $this->assertEquals("Invalid format", $validator->getErrors()->current());
    }

    public function testValidateInvalidProperty()
    {
        $validator = new UrlValidator();
        $urlData = new \stdClass();
        $urlData->invalid = "invalid";
        $validator->initialize('url', $urlData)->validate('url');
        $this->assertEquals(
            "url must contain url property",
            $validator->getErrors()->current()
        );
    }

    public function testValidateExceededStructure()
    {
        $validator = new UrlValidator();
        $urlData = new \stdClass();
        $urlData->url = "url";
        $urlData->invalid = "invalid";
        $validator->initialize('url', $urlData)->validate('url');
        $this->assertEquals(
            "url must contain only url property",
            $validator->getErrors()->current()
        );
    }
}
