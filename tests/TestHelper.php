<?php

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT_PATH', __DIR__);
define('PATH_CONTROLLERS', __DIR__ . '/../app/controllers/');
define('PATH_MODELS', __DIR__ . '/../app/models/');
define('PATH_SERVICES', __DIR__ . '/../app/services/');
define('PATH_VALIDATORS', __DIR__ . '/../app/validators/');

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// Required for phalcon/incubator
include __DIR__ . "/../vendor/autoload.php";

// Use the application autoloader to autoload the classes
// Autoload the dependencies found in composer
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    array(
        ROOT_PATH
    )
);

$loader->registerNamespaces(
    array(
        "Test"    => "/",
        "Test\\Controllers" => ROOT_PATH . "/controllers/",
        "Test\\Models" => ROOT_PATH . "/models/",
        "Test\\Services" => ROOT_PATH . "/services/",
        "Test\\Validators" => ROOT_PATH . "/validators/",
        "Test\\Fixtures" => ROOT_PATH . "/fixtures/",
        "Chaordic\\Services" => ROOT_PATH . "/../app/services/",
        "Chaordic\\Models" => ROOT_PATH . "/../app/models/",
        "Chaordic\\Validators" => ROOT_PATH . "/../app/validators/",
        "Chaordic\\Controllers" => ROOT_PATH . "/../app/controllers/"
    )
);

$loader->register();

$di = new FactoryDefault();
Di::reset();

// Add any needed services to the DI here

Di::setDefault($di);
