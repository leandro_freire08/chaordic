<?php

namespace Test\Fixtures;

class UrlFixture
{
    public static function get()
    {
        return (object) array(
            "url" => "http://test.com",
            "userId" => "test"
        );
    }
}
