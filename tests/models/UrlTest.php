<?php

namespace Test\Model;

use Chaordic\Models\Url;

class UrlTest extends \UnitTestCase
{
    public function testPopulate()
    {
        $urlData = new \stdClass();
        $urlData->url = "http://test.com";
        $urlData->userId = "testuser";

        $url = new Url();
        $url->populate($urlData);

        $this->assertEquals($urlData->url, $url->url);
        $this->assertEquals($urlData->userId, $url->userId);
        $this->assertEquals(0, $url->hits);
        $this->assertNotNull($url->shortUrl);
    }

    public function testPopulateFailure()
    {
        $urlData = new \stdClass();
        $urlData->url = "http://test.com";
        $urlData->userId = "testuser";

        $url = new Url();
        $url->populate($urlData);

        $urlData->url = "http://test2.com";
        $urlData->userId = "testuser2";

        $this->assertNotEquals($urlData->url, $url->url);
        $this->assertNotEquals($urlData->userId, $url->userId);
    }
}
