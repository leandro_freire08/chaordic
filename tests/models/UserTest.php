<?php

namespace Test\Model;

use Chaordic\Models\User;

class UserTest extends \UnitTestCase
{
    public function testPopulate()
    {
        $userData = new \stdClass();
        $userData->id = "test";
        $user = new User();
        $user->populate($userData);

        $this->assertEquals($userData->id, $user->name);
    }

    public function testPopulateFailure()
    {
        $userData = new \stdClass();
        $userData->id = "test";
        $user = new User();
        $user->populate($userData);

        $userData->id = "test2";
        $this->assertNotEquals($userData->id, $user->name);
    }

    public function testCreate()
    {
        $userData = new \stdClass();
        $userData->id = "test";
        $user = new User();
        $user->populate($userData);
        $user->save();
        $this->assertInternalType('string', $user->name);
    }
}
