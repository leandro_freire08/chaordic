<?php

namespace Test\Model;

use Chaordic\Models\Stats;
use Chaordic\Models\Url;
use Test\Fixtures\UrlFixture;

class StatsTest extends \UnitTestCase
{
    public function testGetGlobalStatistics()
    {
        $urlFixture = UrlFixture::get();
        $url = new Url();
        $url->populate($urlFixture);
        $url->save();

        $stats = new Stats();
        $statistics = $stats->getGlobalStatistics();
        $statistics = $statistics[0];
        $this->assertEquals(1, $statistics['urlCount']);
        $this->assertEquals(0, $statistics['hits']);
        $this->assertEquals(1, count($statistics['topUrls']));
    }
}
