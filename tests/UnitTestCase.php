<?php

use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;
use Chaordic\Services\ShortUrlService;

abstract class UnitTestCase extends PhalconTestCase
{
    /**
     * @var \Voice\Cache
     */
    protected $_cache;

    /**
     * @var \Phalcon\Config
     */
    protected $_config;

    /**
     * @var bool
     */
    private $_loaded = false;

    public function setUp()
    {
        parent::setUp();

        // Load any additional services that might be required during testing
        $di = Di::getDefault();

        $config = include __DIR__ . "/../config/config_test.php";

        $di->set('config', function () use ($config) {
            return $config;
        }, true);

        $di->set('mongoClient', function () use ($config) {
            $dbConfig = $config->database->toArray();
            return new MongoClient($dbConfig['host']);
        }, true);

        $di->set('mongo', function () use ($config) {
            $mongoClient = $this->getDI()->get('mongoClient');
            $dbConfig = $config->database->toArray();
            return $mongoClient->selectDB($dbConfig['dbname']);
        }, true);

        $di->set('collectionManager', function () {
            return new Phalcon\Mvc\Collection\Manager();
        }, true);

        $di->set('short', function () {
            return new ShortUrlService();
        }, true);

        $this->setDi($di);

        $this->_loaded = true;
    }

    /**
     * Check if the test case is setup properly
     *
     * @throws \PHPUnit_Framework_IncompleteTestError;
     */
    public function __destruct()
    {
        if (!$this->_loaded) {
            throw new \PHPUnit_Framework_IncompleteTestError('Please run parent::setUp().');
        }
    }

    public function tearDown()
    {
        parent::tearDown();
        $mongoClient = $this->getDI()->get('mongoClient');
        $config = $this->getDI()->get('config');
        $dbConfig = $config->database->toArray();
        $mongoClient->dropDB($dbConfig['dbname']);
    }
}
