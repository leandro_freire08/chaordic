<?php

namespace Test\Services;

use Chaordic\Services\ShortUrlService;

class ShortUrlServiceTest extends \UnitTestCase
{
    public function testShortUrl()
    {
        $url = "http://teste.com";
        $service = new ShortUrlService();
        $code = $service->shortUrl($url);
        $this->assertNotNull($code);
    }

    /**
     * @expectedException \Exception
     */
    public function testShortUrlInvalidUrlFormat()
    {
        $url = "teste.com";
        $service = new ShortUrlService();
        $service->shortUrl($url);
    }

    /**
     * @expectedException \Exception
     */
    public function testShortUrlInvalidUrl()
    {
        $url = "http://urlnotexist.com.br";
        $service = new ShortUrlService();
        $service->shortUrl($url);
    }
}
