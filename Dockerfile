FROM ubuntu:14.04.4
MAINTAINER Leandro Augusto <leandro.freire08@gmail.com>

# Surpress Upstart errors/warnings
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# Disable tty
ENV DEBIAN_FRONTEND noninteractive

#Update base image
RUN apt-get update && \
apt-get install -y software-properties-common

#Add Nginx ppa
RUN add-apt-repository ppa:nginx/stable && \
apt-get update && \
apt-get upgrade -y

#Add PHP5.6 ppa
RUN apt-get install -y language-pack-en-base && \
apt-get install -y python-software-properties && \
LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php5-5.6 && \
apt-get update && \
apt-get update

#Add MongoDB ppa
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN apt-get update && \
BUILD_PACKAGES="supervisor gcc git curl libpcre3-dev nginx php5 php5-apcu php5-cgi php5-cli php5-common php5-curl php5-dev php5-fpm php5-json php5-mongo php5-xdebug mongodb-org" && \
apt-get -y install $BUILD_PACKAGES

# Install Phalcon
RUN git clone --depth=1 git://github.com/phalcon/cphalcon.git /usr/local/src/cphalcon && \
cd /usr/local/src/cphalcon/build && ./install && \
echo "extension=phalcon.so" > /etc/php5/mods-available/phalcon.ini && \
php5enmod phalcon

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# Configure Nginx site
RUN rm -Rf /etc/nginx/conf.d/* && \
rm -Rf /etc/nginx/sites-available/default && \
rm -Rf /etc/nginx/sites-enabled/default
ADD deploy/chaordic /etc/nginx/sites-available/default.conf
RUN ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf

# Tweak nginx config
RUN sed -i -e"s/worker_processes  1/worker_processes 5/" /etc/nginx/nginx.conf && \
sed -i -e"s/keepalive_timeout\s*65/keepalive_timeout 2/" /etc/nginx/nginx.conf && \
sed -i -e"s/keepalive_timeout 2/keepalive_timeout 2;\n\tclient_max_body_size 100m/" /etc/nginx/nginx.conf && \
echo "daemon off;" >> /etc/nginx/nginx.conf

# Tweak php-fpm config
RUN sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php5/fpm/php.ini && \
sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php5/fpm/php.ini && \
sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php5/fpm/php.ini && \
sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php5/fpm/php-fpm.conf && \
sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" /etc/php5/fpm/pool.d/www.conf && \
sed -i -e "s/pm.max_children = 5/pm.max_children = 9/g" /etc/php5/fpm/pool.d/www.conf && \
sed -i -e "s/pm.start_servers = 2/pm.start_servers = 3/g" /etc/php5/fpm/pool.d/www.conf && \
sed -i -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 2/g" /etc/php5/fpm/pool.d/www.conf && \
sed -i -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 4/g" /etc/php5/fpm/pool.d/www.conf && \
sed -i -e "s/pm.max_requests = 500/pm.max_requests = 200/g" /etc/php5/fpm/pool.d/www.conf

# Fix ownership of sock file for php-fpm
RUN sed -i -e "s/;listen.mode = 0660/listen.mode = 0750/g" /etc/php5/fpm/pool.d/www.conf && \
find /etc/php5/cli/conf.d/ -name "*.ini" -exec sed -i -re 's/^(\s*)#(.*)/\1;\2/g' {} \;

# Setup Volume that will contain application
RUN mkdir /chaordic

# SetUp application
WORKDIR /chaordic
COPY . /chaordic
RUN composer update

RUN chown -Rf www-data:www-data /chaordic && \
chmod 755 /chaordic -R

# Supervisor Config
ADD deploy/supervisord.conf /etc/supervisord.conf

# Start Supervisord
ADD deploy/start.sh /start.sh
RUN chmod 755 /start.sh

# Create the MongoDB data directory
RUN mkdir -p /data/db

# Clean up
RUN apt-get remove --purge -y software-properties-common && \
apt-get autoremove -y && \
apt-get clean && \
apt-get autoclean

# Expose ports
EXPOSE 80
EXPOSE 27017

CMD ["/bin/bash", "/start.sh"]