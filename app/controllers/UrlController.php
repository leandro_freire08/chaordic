<?php

namespace Chaordic\Controllers;

use Chaordic\Models\Url;

class UrlController extends ApplicationController
{
    public function redirectAction($id)
    {
        $urlId = null;
        try {
            $urlId = new \MongoId($id);
        } catch (\MongoException $e) {
            return $this->buildResponse(500, $e->getMessage());
        }

        $url = Url::findById($urlId);
        if (!$url) {
            return $this->buildResponse(404);
        }
        $url->hits++;
        $url->save();
        return $this->response->redirect($url->url, true, 301);
    }

    public function destroyAction($id)
    {
        $urlId = null;
        try {
            $urlId = new \MongoId($id);
        } catch (\MongoException $e) {
            return $this->buildResponse(500, $e->getMessage());
        }

        $url = Url::findById($urlId);

        if ($url) {
            $url->delete();
            return $this->buildResponse(204);
        } else {
            return $this->buildResponse(404);
        }
    }
}
