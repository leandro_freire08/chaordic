<?php

namespace Chaordic\Controllers;

use Chaordic\Models\Url;
use Chaordic\Models\User;
use Chaordic\Validators\UrlValidator;
use Chaordic\Validators\UserValidator;

class UserController extends ApplicationController
{
    public function createAction()
    {
        $userData = $this->request->getJsonRawBody();
        $validator = new UserValidator();
        $validator->initialize(User::class, $userData)->validate("id");

        if ($validator->getErrors()->count()) {
            return $this->buildResponse(400, $validator->getErrors());
        }

        $user = new User();
        if ($user->populate($userData)) {
            $user->save();
            return $this->buildResponse(201);
        }

        return $this->buildResponse(500);
    }

    public function createUrlAction($userId)
    {
        $user = User::find(array(array('name' => $userId)));

        if (empty($user)) {
            return $this->buildResponse(404);
        }

        $urlData = $this->request->getJsonRawBody();
        $validator = new UrlValidator();
        $validator->initialize(Url::class, $urlData)->validate("url");

        if ($validator->getErrors()->count()) {
            return $this->buildResponse(400, $validator->getErrors());
        }

        $url = new Url();
        $urlData->userId = $userId;

        if ($url->populate($urlData)) {
            $url->save();
            return $this->buildResponse(201, $url);
        }

        return $this->buildResponse(500);
    }

    public function statsAction($userName)
    {
        $statistics = Url::find(array(array('userId' => $userName)));

        if (empty($statistics)) {
            return $this->buildResponse(404);
        }

        foreach ($statistics as $key => $url) {
            $url = $url->toArray();
            $url['id'] = $url['_id']->__toString();
            unset($url['_id']);
            $statistics[$key] = $url;
        }

        return $this->buildResponse(200, $statistics);
    }

    public function destroyAction($id)
    {
        $userId = null;
        try {
            $userId = new \MongoId($id);
        } catch (\MongoException $e) {
            return $this->buildResponse(500, $e->getMessage());
        }

        $user = User::findById($userId);

        if ($user) {
            $user->delete();
            return $this->buildResponse(204);
        } else {
            return $this->buildResponse(404);
        }
    }
}
