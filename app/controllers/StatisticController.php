<?php

namespace Chaordic\Controllers;

use Chaordic\Models\Stats;
use Chaordic\Models\Url;

class StatisticController extends ApplicationController
{
    public function indexAction()
    {
        $stats = new Stats();
        $statistics = $stats->getGlobalStatistics();

        return $this->buildResponse(200, $statistics);
    }

    public function showAction($id)
    {
        $urlId = null;
        try {
            $urlId = new \MongoId($id);
        } catch (\MongoException $e) {
            return $this->buildResponse(500, $e->getMessage());
        }

        $statistic = Url::findById($urlId);
        $statistic = $statistic->toArray();
        $statistic['id'] = $statistic['_id']->__toString();
        unset($statistic['_id']);

        return $this->buildResponse(200, $statistic);
    }
}
