<?php

namespace Chaordic\Controllers;

use Phalcon\Mvc\Controller;

class ApplicationController extends Controller
{
    public function buildResponse($status, $data = null)
    {
        $this->response->setJsonContent(
            array(
                'status' => $status,
                'data'   => $data
            )
        );

        $this->response->setStatusCode($status);
        $this->response->setContentType('application/json');
        return $this->response;
    }
}
