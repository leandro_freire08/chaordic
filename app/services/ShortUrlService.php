<?php

namespace Chaordic\Services;

class ShortUrlService
{
    protected static $chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
    protected static $shortPrefix = "http://short.url/";

    public function shortUrl($url)
    {

        if ($this->validateUrlFormat($url) == false) {
            throw new \Exception("Invalid url format.");
        }

        if (!$this->verifyUrlExists($url)) {
            throw new \Exception("Invalid url address.");
        }

        return $this->createShortCode();
    }

    protected function validateUrlFormat($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED);
    }

    protected function verifyUrlExists($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }

    protected function createShortCode()
    {
        return self::$shortPrefix . substr(str_shuffle(self::$chars), 0, 5);
    }
}
