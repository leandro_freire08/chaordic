<?php

namespace Chaordic\Models;

use Phalcon\Mvc\Collection;

class User extends Collection implements ModelInterface
{
    public $name;

    public function populate(\stdClass $userData)
    {
        if (!is_string($userData->id)) {
            return false;
        }
        $this->name = $userData->id;

        return true;
    }

    public function afterCreate()
    {
        $this->_id = $this->_id->__toString();
    }
}
