<?php

namespace Chaordic\Models;

use Phalcon\Mvc\Collection;

class Stats extends Collection
{
    public function getGlobalStatistics()
    {
        $statistics = Url::aggregate(
            array(
                array(
                    '$project' => array(
                        '_id' => 1,
                        'hits' => 1,
                        'url' => 1,
                        'shortUrl' => 1
                    )
                ),
                array (
                    '$sort' => array(
                        'hits' => -1
                    )
                ),
                array(
                    '$group' => array(
                        '_id' => '$id',
                        'totalHits' => array('$sum' => '$hits'),
                        'urlCount' => array('$sum' => 1),
                        'urls' => array(
                            '$push' => array(
                                'id' => '$_id',
                                'hits' => '$hits',
                                'url' => '$url',
                                'shortUrl' => '$shortUrl'
                            )
                        )
                    )
                ),
                array(
                    '$project' => array(
                        '_id' => 0,
                        'hits' => '$totalHits',
                        'urlCount' => '$urlCount',
                        'topUrls' => array(
                            '$slice' => array('$urls', 10)
                        )
                    )
                )
            )
        );

        foreach ($statistics['result'][0]['topUrls'] as $key => $url) {
            $url['id'] = $url['id']->__toString();
            $statistics['result'][0]['topUrls'][$key] = $url;
        }
        return $statistics['result'];
    }
}
