<?php

namespace Chaordic\Models;

interface ModelInterface
{
    public function populate(\stdClass $data);
}
