<?php

namespace Chaordic\Models;

use Phalcon\Mvc\Collection;

class Url extends Collection implements ModelInterface
{
    public $url;
    public $shortUrl;
    public $userId;
    public $hits;

    public function populate(\stdClass $urlData)
    {
        if (!is_string($urlData->url) || !is_string($urlData->userId)) {
            return false;
        }
        
        $this->url = $urlData->url;
        $this->userId = $urlData->userId;
        $this->shortUrl = $this->getDI()->get('short')->shortUrl($urlData->url);
        $this->hits = 0;

        return true;
    }

    public function afterCreate()
    {
        $this->_id = $this->_id->__toString();
    }
}
