<?php

namespace Chaordic\Validators;

interface ValidatorInterface
{
    public function initialize($entity, \stdClass $data);
}
