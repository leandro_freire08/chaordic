<?php

namespace Chaordic\Validators;

abstract class AbstractValidator
{
    protected $errors;
    protected $data;
    protected $entity;

    public function validate($property)
    {
        $this->validData();
        $this->validateStructure($property);
    }

    private function validData()
    {
        if (count((array) $this->data) == 0) {
            $this->errors->append("Invalid format");
        }
    }

    private function validateStructure($property)
    {
        if (!property_exists($this->data, $property)) {
            $this->errors->append($this->entity . " must contain ${property} property");
        }

        if (count((array) $this->data) > 1) {
            $this->errors->append($this->entity . " must contain only ${property} property");
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
