<?php

namespace Chaordic\Validators;

class UserValidator extends AbstractValidator implements ValidatorInterface
{
    public function initialize($entity, \stdClass $data)
    {
        $this->entity = $entity;
        $this->data = $data;
        $this->errors = new \ArrayIterator();
        return $this;
    }
}
