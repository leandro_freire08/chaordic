<?php

use Phalcon\Mvc\Micro\Collection as MicroCollection;

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Add your routes here
 */

$urls = new MicroCollection();
$urls->setHandler('Chaordic\Controllers\UrlController', true);
$urls->setPrefix('/url');
$urls->get('/{id}', 'redirectAction');
$urls->delete('/{id}', 'destroyAction');

$app->mount($urls);

$users = new MicroCollection();
$users->setHandler('Chaordic\Controllers\UserController', true);
$users->setPrefix('/user');
$users->post('/', 'createAction');
$users->post('/{userId}/urls', 'createUrlAction');
$users->get('/{userId}/stats', 'statsAction');
$users->delete('/{userId}', 'destroyAction');

$app->mount($users);

$stats = new MicroCollection();
$stats->setHandler('Chaordic\Controllers\StatisticController', true);
$stats->setPrefix('/stats');
$stats->get('/', 'indexAction');
$stats->get('/{id}', 'showAction');

$app->mount($stats);

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
});
